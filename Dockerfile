FROM golang:alpine as builder

LABEL maintainer="Islam Shehata <schehata91@gmail.com>"

WORKDIR /go/src/gitlab.com/ishehata/gitlab-pipeline-trigger

COPY . .

RUN apk update && apk add --no-cache git ca-certificates && update-ca-certificates

RUN CGO_ENABLED=0 GOOS=linux go build -a  -installsuffix cgo -o main .


FROM alpine

WORKDIR /trigger

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

COPY --from=builder /go/src/gitlab.com/ishehata/gitlab-pipeline-trigger/main .

CMD ["./main"]